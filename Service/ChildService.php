<?php

namespace App\Service;

use App\Entity\Child;
use App\Helper\Validator;
use App\Repository\ChildRepository;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Exception;

class ChildService
{
    public function __construct(
        private ChildRepository $childRepository,
        private SerializerInterface $serializer,
    )
    {}

    /**
     * @return array
     */
    public function getAll(): array
    {
        $children = $this->childRepository->findBy(
            [],
            ['id' => 'DESC']
        );

        return array_map(
            function (Child $child) {
                return $child->toArray();
            },
            $children
        );
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function getById($id): array
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $child = $this->childRepository->findOne($id);

        return $child->toArray();
    }

    /**
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function add(string $request): array
    {
        try {
            $child = $this->serializer->deserialize(
                $request,
                'App\Entity\Child',
                'json',
            );
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        (new Validator())->validate($child);

        $newChild = $this->childRepository->save($child);

        return $newChild->toArray();
    }

    /**
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function addMultiple(string $request): array
    {
        try {
            $children = $this->serializer->deserialize(
                $request,
                'App\Entity\Child[]',
                'json',
            );
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        (new Validator())->validateArray($children);

        $newChildren = $this->childRepository->saveMultiple($children);

        return array_map(function ($child) {
            return $child->toArray();
        }, $newChildren);
    }

    /**
     * @param string $id
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function update(string $id, string $request): array
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $child = $this->childRepository->findOne($id);

        try {
            $this->serializer->deserialize(
                $request,
                'App\Entity\Child',
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $child]
            );
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        $updatedChild = $this->childRepository->save($child);

        return $updatedChild->toArray();
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function delete(string $id)
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $child = $this->childRepository->findOne($id);

        $this->childRepository->remove($child);
    }

}