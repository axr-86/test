<?php

namespace App\Service;

use App\Entity\Visitor;
use App\Helper\Validator;
use App\Repository\VisitorRepository;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Exception;

class VisitorService
{
    public function __construct(
        private VisitorRepository $visitorRepository,
        private SerializerInterface $serializer,
    )
    {}

    /**
     * @return array
     */
    public function getAll(): array
    {
        $children = $this->visitorRepository->findBy(
            [],
            ['id' => 'DESC']
        );

        return array_map(
            function (Visitor $visitor) {
                return $visitor->toArray();
            },
            $children
        );
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function getById($id): array
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $visitor = $this->visitorRepository->findOne($id);

        return $visitor->toArray();
    }

    /**
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function add(string $request): array
    {
        try {
            $visitor = $this->serializer->deserialize(
                $request,
                'App\Entity\Visitor',
                'json',
            );
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        (new Validator())->validate($visitor);

        $newVisitor = $this->visitorRepository->save($visitor);

        return $newVisitor->toArray();
    }

    /**
     * @param string $id
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function update(string $id, string $request): array
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $visitor = $this->visitorRepository->findOne($id);

        try {
            $this->serializer->deserialize(
                $request,
                'App\Entity\Visitor',
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $visitor]
            );
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        $updatedVisitor = $this->visitorRepository->save($visitor);

        return $updatedVisitor->toArray();
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function delete(string $id)
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $visitor = $this->visitorRepository->findOne($id);

        $this->visitorRepository->remove($visitor);
    }

}