<?php

namespace App\Service;

use App\Entity\ChildVisitor;
use App\Repository\ChildRepository;
use App\Repository\ChildVisitorRepository;
use App\Repository\VisitorRepository;
use Exception;

class ChildVisitorService
{
    public function __construct(
        private ChildRepository $childRepository,
        private VisitorRepository $visitorRepository,
        private ChildVisitorRepository $childVisitorRepository,
    )
    {}

    /**
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function add(string $request): array
    {
        $data = json_decode($request, true);

        if (!isset($data['child']) || !isset($data['visitor'])) {
            throw new Exception('Отсутствуют необходимые данные.');
        }

        $count = $this->childVisitorRepository->count([
            'child'     => $data['child'],
            'visitor'   => $data['visitor']
        ]);

        if ($count) {
            throw new Exception('Запись уже существует');
        }

        try {
            $child      = $this->childRepository->findOne($data['child']);
            $visitor    = $this->visitorRepository->findOne($data['visitor']);

            $comment = isset($data['comment'])
                ? $data['comment']
                : '';

            $childVisitor = new ChildVisitor();
            $childVisitor->setChild($child);
            $childVisitor->setVisitor($visitor);
            $childVisitor->setComment($comment);
        } catch (Exception) {
            throw new Exception('Ошибка при обработке данных.');
        }

        $newChildVisitor = $this->childVisitorRepository->save($childVisitor);

        return $newChildVisitor->toArray();
    }

    /**
     * @param string $id
     * @param string $request
     * @return array
     * @throws Exception
     */
    public function update(string $id, string $request): array
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $childVisitor = $this->childVisitorRepository->findOne($id);
        $data = json_decode($request, true);

        if (isset($data['comment'])) {
            $childVisitor->setComment($data['comment']);
        }

        $updatedChild = $this->childVisitorRepository->save($childVisitor);

        return $updatedChild->toArray();
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function delete(string $id)
    {
        if (!$id) {
            throw new Exception('ID не должен быть пустым.');
        }

        $child = $this->childVisitorRepository->findOne($id);

        $this->childVisitorRepository->remove($child);
    }

}