<?php

namespace App\Repository;

use App\Entity\Visitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Visitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visitor[]    findAll()
 * @method Visitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitorRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, Visitor::class);
    }

    /**
     * @param int $id
     * @return Visitor
     * @throws Exception
     */
    public function findOne(int $id): Visitor
    {
        $visitor = $this->find($id);

        if (!$visitor) {
            throw new Exception('Ошибка при получении данных.');
        }

        return $visitor;
    }

    /**
     * @param Visitor $visitor
     * @return Visitor
     * @throws Exception
     */
    public function save(Visitor $visitor): Visitor
    {
        try {
            $this->manager->persist($visitor);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $visitor;
    }

    /**
     * @param Visitor $visitor
     * @throws Exception
     */
    public function remove(Visitor $visitor): void
    {
        try {
            $this->manager->remove($visitor);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
