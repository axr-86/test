<?php

namespace App\Repository;

use App\Entity\Child;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Child|null find($id, $lockMode = null, $lockVersion = null)
 * @method Child|null findOneBy(array $criteria, array $orderBy = null)
 * @method Child[]    findAll()
 * @method Child[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChildRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, Child::class);
    }

    /**
     * @param int $id
     * @return Child
     * @throws Exception
     */
    public function findOne(int $id): Child
    {
        $child = $this->find($id);

        if (!$child) {
            throw new Exception('Ошибка при получении данных.');
        }

        return $child;
    }

    /**
     * @param Child $child
     * @return Child
     * @throws Exception
     */
    public function save(Child $child): Child
    {
        try {
            $this->manager->persist($child);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $child;
    }

    /**
     * @param array $children
     * @return Child[]
     * @throws Exception
     */
    public function saveMultiple(array $children): array
    {
        try {
            array_map(function ($child) {
                $this->manager->persist($child);
            }, $children);

            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $children;
    }
    
    /**
     * @param Child $child
     * @throws Exception
     */
    public function remove(Child $child): void
    {
        try {
            $this->manager->remove($child);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
