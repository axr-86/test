<?php

namespace App\Repository;

use App\Entity\ChildVisitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method ChildVisitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChildVisitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChildVisitor[]    findAll()
 * @method ChildVisitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChildVisitorRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, ChildVisitor::class);
    }

    /**
     * @param int $id
     * @return ChildVisitor
     * @throws Exception
     */
    public function findOne(int $id): ChildVisitor
    {
        $childVisitor = $this->find($id);

        if (!$childVisitor) {
            throw new Exception('Ошибка при получении данных.');
        }

        return $childVisitor;
    }

    /**
     * @param ChildVisitor $childVisitor
     * @return ChildVisitor
     * @throws Exception
     */
    public function save(ChildVisitor $childVisitor): ChildVisitor
    {
        try {
            $this->manager->persist($childVisitor);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $childVisitor;
    }

    /**
     * @param ChildVisitor $childVisitor
     * @throws Exception
     */
    public function remove(ChildVisitor $childVisitor): void
    {
        try {
            $this->manager->remove($childVisitor);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
