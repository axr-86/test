<?php

namespace App\Controller;

use App\Service\VisitorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;

class VisitorController extends AbstractController
{
    public function __construct(private VisitorService $visitorService)
    {}

    /**
     * @return JsonResponse
     */
    #[Route('/visitors', name: 'visitors', methods: ['GET'])]
    public function getAll(): JsonResponse
    {
        return new JsonResponse(
            $this->visitorService->getAll(),
            Response::HTTP_OK
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/visitor/{id}', name: 'visitor_get_by_id', methods: ['GET'])]
    public function getById(string $id): JsonResponse
    {
        $visitor = $this->visitorService->getById($id);

        return new JsonResponse(
            $visitor,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/visitor', name: 'visitor_add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        $visitor = $this->visitorService->add($request->getContent());

        return new JsonResponse(
            $visitor,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/visitor/{id}', name: 'visitor_update', methods: ['PATCH'])]
    public function update(string $id, Request $request): JsonResponse
    {
        $visitor = $this->visitorService->update($id, $request->getContent());

        return new JsonResponse(
            $visitor,
            Response::HTTP_OK
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/visitor/{id}', name: 'visitor_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $this->visitorService->delete($id);

        return new JsonResponse(
            ['Информация успешно удалена из БД.'],
            Response::HTTP_OK
        );
    }

}
