<?php

namespace App\Controller;

use App\Service\ChildVisitorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;

class ChildVisitorController extends AbstractController
{
    public function __construct(private ChildVisitorService $childVisitorService)
    {}

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child_visitor', name: 'child_visitor_add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        $childVisitor = $this->childVisitorService->add($request->getContent());

        return new JsonResponse(
            $childVisitor,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child_visitor/{id}', name: 'child_visitor_update', methods: ['PATCH'])]
    public function update(string $id, Request $request): JsonResponse
    {
        $visitor = $this->childVisitorService->update($id, $request->getContent());

        return new JsonResponse(
            $visitor,
            Response::HTTP_OK
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child_visitor/{id}', name: 'child_visitor_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $this->childVisitorService->delete($id);

        return new JsonResponse(
            ['Информация успешно удалена из БД.'],
            Response::HTTP_OK
        );
    }

}
