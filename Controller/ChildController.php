<?php

namespace App\Controller;

use App\Service\ChildService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;

class ChildController extends AbstractController
{
    public function __construct(private ChildService $childService)
    {}

    /**
     * @return JsonResponse
     */
    #[Route('/children', name: 'children', methods: ['GET'])]
    public function getAll(): JsonResponse
    {
        return new JsonResponse(
            $this->childService->getAll(),
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/children', name: 'children_add', methods: ['POST'])]
    public function addMultiple(Request $request): JsonResponse
    {
        $child = $this->childService->addMultiple($request->getContent());

        return new JsonResponse(
            $child,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child/{id}', name: 'child_get_by_id', methods: ['GET'])]
    public function getById(string $id): JsonResponse
    {
        $child = $this->childService->getById($id);

        return new JsonResponse(
            $child,
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child', name: 'child_add', methods: ['POST'])]
    public function add(Request $request): JsonResponse
    {
        $child = $this->childService->add($request->getContent());

        return new JsonResponse(
            $child,
            Response::HTTP_CREATED
        );
    }

    /**
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child/{id}', name: 'child_update', methods: ['PATCH'])]
    public function update(string $id, Request $request): JsonResponse
    {
        $child = $this->childService->update($id, $request->getContent());

        return new JsonResponse(
            $child,
            Response::HTTP_OK
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    #[Route('/child/{id}', name: 'child_delete', methods: ['DELETE'])]
    public function delete(string $id): JsonResponse
    {
        $this->childService->delete($id);

        return new JsonResponse(
            ['Информация успешно удалена из БД.'],
            Response::HTTP_OK
        );
    }

}
