<?php

namespace App\Helper;

use Exception;
use Symfony\Component\Validator\Validation;

class Validator
{

    /**
     * @param object $object
     * @throws Exception
     */
    public function validate(object $object): void
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $errors = $validator->validate($object);

        if (count($errors) > 0) {
            throw new Exception((string) $errors);
        }
    }

    /**
     * @param array $array
     * @throws Exception
     */
    public function validateArray(array $array): void
    {
        $notValidated = [];

        array_map(function ($object) use (&$notValidated){
            if (is_object($object)) {
                $this->validate($object);
            } else {
                $notValidated[] = $object;
            }
        }, $array);

        if (count($notValidated) > 0) {
            throw new Exception('Присутствуют невалидированные объекты.');
        }
    }

}