<?php

namespace App\Entity;

use App\Repository\ChildRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChildRepository::class)]
class Child
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'Имя не должно быть пустым.')]
    private $name;

    #[ORM\OneToMany(mappedBy: 'child', targetEntity: ChildVisitor::class)]
    private $visitors;

    public function __construct()
    {
        $this->visitors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ChildVisitor[]
     */
    public function getVisitors(): Collection
    {
        return $this->visitors;
    }

    public function addVisitor(ChildVisitor $visitor): self
    {
        if (!$this->visitors->contains($visitor)) {
            $this->visitors[] = $visitor;
            $visitor->setChild($this);
        }

        return $this;
    }

    public function removeVisitor(ChildVisitor $visitor): self
    {
        if ($this->visitors->removeElement($visitor)) {
            if ($visitor->getChild() === $this) {
                $visitor->setChild(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        $visitors = $this->visitors->map(
            function ($childVisitor) {
                return [
                    'id'        => $childVisitor->getVisitor()->getId(),
                    'name'      => $childVisitor->getVisitor()->getName(),
                    'comment'   => $childVisitor->getComment(),
                ];
            }
        );

        return [
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'visitors'  => $visitors->toArray(),
        ];
    }

    public function __toString(): string
    {
        return $this->getName();
    }

}
