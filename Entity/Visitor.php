<?php

namespace App\Entity;

use App\Repository\VisitorRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VisitorRepository::class)]
class Visitor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'Имя не должно быть пустым.')]
    private $name;

    #[ORM\OneToMany(mappedBy: 'visitor', targetEntity: ChildVisitor::class)]
    private $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ChildVisitor[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(ChildVisitor $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setVisitor($this);
        }

        return $this;
    }

    public function removeChild(ChildVisitor $child): self
    {
        if ($this->children->removeElement($child)) {
            if ($child->getVisitor() === $this) {
                $child->setVisitor(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        $children = $this->children->map(
            function ($childVisitor) {
                return [
                    'id'        => $childVisitor->getChild()->getId(),
                    'name'      => $childVisitor->getChild()->getName(),
                    'comment'   => $childVisitor->getComment(),
                ];
            }
        );

        return [
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'children'  => $children->toArray(),
        ];
    }

    public function __toString(): string
    {
        return $this->getName();
    }

}
