<?php

namespace App\Entity;

use App\Repository\ChildVisitorRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChildVisitorRepository::class)]
#[ORM\UniqueConstraint(name: 'child_visitor_idx', columns: ['child_id', 'visitor_id'])]
class ChildVisitor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Child::class, inversedBy: 'visitors', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, unique: true)]
    private $child;

    #[ORM\ManyToOne(targetEntity: Visitor::class, inversedBy: 'children', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false, unique: true)]
    private $visitor;

    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChild(): ?Child
    {
        return $this->child;
    }

    public function setChild(?Child $child): self
    {
        $this->child = $child;

        return $this;
    }

    public function getVisitor(): ?Visitor
    {
        return $this->visitor;
    }

    public function setVisitor(?Visitor $visitor): self
    {
        $this->visitor = $visitor;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id'      => $this->getId(),
            'child'   => [
                'id'    => $this->getChild()->getId(),
                'name'  => $this->getChild()->getName(),
            ],
            'visitor' => [
                'id'    => $this->getVisitor()->getId(),
                'name'  => $this->getVisitor()->getName(),
            ],
            'comment' => $this->getComment(),
        ];
    }

}
